// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract RODToken is ERC20, ERC20Burnable, Ownable, AccessControl, ReentrancyGuard {
    
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    uint256 private swapMin;
    uint256 private swapFee;
    address private MGRCA;
    address private NFTCA;
    mapping(address => Swap) private swaps;

    struct Swap {
        uint256 rod;
        uint256 mgr;
    }

    event Swapped(address mgrHolder, uint256 mgr, address rodHolder, uint256 rod);

    constructor() ERC20("ROD Token", "ROD") {
        swapMin = 10 ** 15;
        swapFee = 500;
        _transferOwnership(_msgSender());
        setAdminRole(_msgSender());
    }

    function setAdminRole(address to) public onlyOwner {
        _grantRole(ADMIN_ROLE, to);
    }

    function revokeAdminRole(address to) public onlyOwner {
        _revokeRole(ADMIN_ROLE, to);
    }

    function setOptionsForSwap(
        uint256 _swapMin,
        uint256 _swapFee
    ) public onlyOwner returns (uint256, uint256) {
        swapMin = _swapMin;
        swapFee = _swapFee;

        return (swapMin, swapFee);
    }

    function getOptionsForSwap() public view returns (uint256, uint256) {
        return (swapMin, swapFee);
    }

    function setCA(
        address _MGRCA,
        address _NFTCA
    ) public onlyOwner {

        if (MGRCA != _MGRCA) {
            revokeAdminRole(MGRCA);
            setAdminRole(_MGRCA);

            MGRCA = _MGRCA;
        }

        if (NFTCA != _NFTCA) {
            revokeAdminRole(NFTCA);
            setAdminRole(_NFTCA);

            NFTCA = _NFTCA;
        }
    }

    function getCA() public view returns (address, address) {
        return (MGRCA, NFTCA);
    }

    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        require(balanceOf(from) - amount >= allowance(from, owner()), "transfer amount cannot be bigger than swap allowance");

        super._transfer(from, to, amount);
    }

    function mintTokenByAdmin(
        address to,
        uint256 amount
    ) public onlyRole(ADMIN_ROLE) {

        _mint(to, amount);
    }

    function releaseHold(
        address holder
    ) internal {
        swaps[holder] = Swap(0, 0);
        _approve(holder, owner(), 0);
    }

    function transferByAdmin(
        address from,
        address to,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) {
        require(balanceOf(from) >= rod, "not enough ROD");

        _transfer(from, to, rod);
    }

    function transferOnMarketByAdmin(
        uint256 tokenId,
        address buyer,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) {
        require(balanceOf(buyer) >= rod, "not enough ROD");

        (bool transferResult, bytes memory transferData) = address(NFTCA).call(abi.encodeWithSignature("transferOnMarketByRODCA(uint256,address,uint256)", tokenId, buyer, rod));
        require(transferResult, "failed to call transferOnMarketByRod()");

        (uint256 forOwner, uint256 forSeller, address seller) = abi.decode(transferData, (uint256, uint256, address));

        if (forOwner > 0) {
            transferByAdmin(buyer, owner(), forOwner);
        }

        transferByAdmin(buyer, seller, forSeller);
    }

    function bidOnMarket(
        uint256 tokenId,
        uint256 rod
    ) public nonReentrant {
        address sender = _msgSender();
        require(balanceOf(sender) >= rod, "not enough ROD to bid");
        
        (bool bidResult, bytes memory bidData) = address(NFTCA).call(abi.encodeWithSignature("bidOnMarketByRODCA(uint256,address,uint256)", tokenId, sender, rod));
        require(bidResult, "failed to call bidOnMarketByRODCA() in NFT contract");
        (address topBidder, uint256 topBidAmount, address manager) = abi.decode(bidData, (address, uint256, address));

        uint256 forManager = rod - topBidAmount;

        transfer(manager, forManager);

        if (topBidAmount > 0) {
            transfer(topBidder, topBidAmount);
        }
    }
    
    function holdToSwap(
        uint256 rod,
        uint256 requiredMgr
    ) public nonReentrant {
        address sender = _msgSender();
        require(allowance(sender, owner()) == 0, "already held to swap");
        require(rod > 0, "ROD must bigger than 0 for swap");
        require(rod >= swapMin, "ROD must bigger than min for swap");
        require(balanceOf(sender) >= rod, "not enough ROD to hold");

        (bool optionResult, bytes memory optionData) = address(MGRCA).call(abi.encodeWithSignature("getOptionsForSwap()"));
        require(optionResult, "failed to call getOptionsForSwap() in MGR");
        (uint256 mgrSwapMin,) = abi.decode(optionData, (uint256, uint256));

        require(requiredMgr >= mgrSwapMin, "MGR must bigger than minimum value for swap");

        swaps[sender] = Swap(rod, requiredMgr);
        _approve(sender, owner(), rod);
    }

    function releaseHoldByHolder() public {
        address holder = _msgSender();
        require(allowance(holder, owner()) > 0, "cannot find swap");

        swaps[holder] = Swap(0, 0);
        _approve(holder, owner(), 0);
    }

    function getSwap(
        address holder
    ) public view returns (uint256, uint256) {
        return (swaps[holder].rod, swaps[holder].mgr);
    }

    function checkSwap(
        address holder,
        uint256 rod,
        uint256 mgr
    ) public onlyRole(ADMIN_ROLE) view returns (uint256) {
        uint256 allowedRod = allowance(holder, owner());
        require(allowedRod > 0, "no allowance for swap");
        require(allowedRod >= swapMin, "allowance must bigger than min for swap");
        require(balanceOf(holder) >= allowedRod, "not enough ROD to swap");

        uint256 heldRod = swaps[holder].rod;
        uint256 requiredMgr = swaps[holder].mgr;

        require(heldRod == rod, "incorrect rod value for MGR holder");
        require(allowedRod == heldRod, "invalid allowance and amount for swap");
        require(requiredMgr == mgr, "incorrect MGR value for rod holder");

        return allowedRod;
    }

    function swapByMGRCA(
        address rodHolder,
        uint256 rod,
        address mgrHolder,
        uint256 mgr
    ) public onlyRole(ADMIN_ROLE) {
        uint256 allowedRod = checkSwap(rodHolder, rod, mgr);
        
        uint256 forOwner = allowedRod * swapFee / 10000;
        uint256 forMgrHolder = allowedRod - forOwner;

        releaseHold(rodHolder);

        if (forOwner > 0) {
            _transfer(rodHolder, owner(), forOwner);
        }

        _transfer(rodHolder, mgrHolder, forMgrHolder);
        
        emit Swapped(mgrHolder, mgr, rodHolder, rod);
    }

    function swap(
        address mgrHolder,
        uint256 mgr,
        uint256 rod
    ) public nonReentrant {
        address rodHolder = _msgSender();
        require(mgrHolder != rodHolder, "cannot swap myself");
        require(mgrHolder != address(0), "invalid address to transfer");
        require(rod >= swapMin, "rod must bigger than minimum value for swap");
        require(balanceOf(rodHolder) >= rod, "not enough ROD to swap");
        
        (bool swapResult,) = address(MGRCA).call(abi.encodeWithSignature("swapByRODCA(address,uint256,address,uint256)", mgrHolder, mgr, rodHolder, rod));

        require(swapResult, "failed to call swapByRODCA() in MGR contract");
        
        uint256 forOwner = rod * swapFee / 10000;
        uint256 forMgrHolder = rod - forOwner;

        if (forOwner > 0) {
            transfer(owner(), forOwner);
        }

        transfer(mgrHolder, forMgrHolder);
        
        emit Swapped(mgrHolder, mgr, rodHolder, rod);
    }

}