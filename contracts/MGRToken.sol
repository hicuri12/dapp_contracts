// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract MGRToken is ERC20, Ownable, AccessControl, ReentrancyGuard {
        
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    uint256 private swapMin;
    uint256 private swapFee;
    address private RODCA;
    address private NFTCA;
    mapping(address => bool) private lockups;
    mapping(address => Swap) private swaps;

    struct Swap {
        uint256 mgr;
        uint256 rod;
    }

    event Swapped(address mgrHolder, uint256 mgr, address rodHolder, uint256 rod);

    constructor() ERC20("MGR Token", "MGR") {
        swapMin = 10 ** 15;
        swapFee = 250;
        _transferOwnership(_msgSender());
        setAdminRole(_msgSender());
        _mint(owner(), 10000000000 * 10 ** decimals());
    }

    function setAdminRole(address to) public onlyOwner {
        _grantRole(ADMIN_ROLE, to);
    }

    function revokeAdminRole(address to) public onlyOwner {
        _revokeRole(ADMIN_ROLE, to);
    }

    function setCA(
        address _RODCA,
        address _NFTCA
    ) public onlyOwner {

        if (RODCA != _RODCA) {
            revokeAdminRole(RODCA);
            setAdminRole(_RODCA);

            RODCA = _RODCA;
        }

        if (NFTCA != _NFTCA) {
            revokeAdminRole(NFTCA);
            setAdminRole(_NFTCA);

            NFTCA = _NFTCA;
        }
    }

    function getCA() public view returns (address, address) {
        return (RODCA, NFTCA);
    }

    function setLockup(
        address holder,
        uint256 status
    ) public onlyOwner {
        require(status < 2, "invalid status value");
        bool lockupStatus = false;

        if (status == 1) {
            lockupStatus = true;
        }
        lockups[holder] = lockupStatus;
    }

    function isLockedUp(
        address holder
    ) public view returns (bool) {
        return lockups[holder];
    }

    function setOptionsForSwap(
        uint256 _swapMin,
        uint256 _swapFee
    ) public onlyOwner {
        swapMin = _swapMin;
        swapFee = _swapFee;
    }

    function getOptionsForSwap() public view returns (uint256, uint256) {
        return (swapMin, swapFee);
    }

    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        require(!lockups[from], "holder locked up");
        require(balanceOf(from) - amount >= allowance(from, owner()), "transfer amount cannot be bigger than swap allowance");

        super._transfer(from, to, amount);
    }

    function releaseHold(
        address holder
    ) internal {
        swaps[holder] = Swap(0, 0);
        _approve(holder, owner(), 0);
    }

    function holdToSwap(
        uint256 mgr,
        uint256 requiredRod
    ) public nonReentrant {
        address sender = _msgSender();
        require(allowance(sender, owner()) == 0, "already held to swap");
        require(mgr > 0, "mgr must bigger than 0 for swap");
        require(mgr >= swapMin, "mgr must bigger than min for swap");
        require(balanceOf(sender) >= mgr, "not enough MGR to hold");

        (bool optionResult, bytes memory optionData) = address(RODCA).call(abi.encodeWithSignature("getOptionsForSwap()"));
        require(optionResult, "failed to call getOptionsForSwap() in ROD");
        (uint256 rodSwapMin,) = abi.decode(optionData, (uint256, uint256));

        require(requiredRod >= rodSwapMin, "rod must bigger than minimum value for swap");

        swaps[sender] = Swap(mgr, requiredRod);
        _approve(sender, owner(), mgr);
    }

    function releaseHoldByHolder() public {
        address holder = _msgSender();
        require(allowance(holder, owner()) > 0, "cannot find swap");

        swaps[holder] = Swap(0, 0);
        _approve(holder, owner(), 0);
    }

    function getSwap(
        address holder
    ) public view returns (uint256, uint256) {
        return (swaps[holder].mgr, swaps[holder].rod);
    }

    function checkSwap(
        address holder,
        uint256 mgr,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) view returns (uint256) {
        uint256 allowedMgr = allowance(holder, owner());
        require(allowedMgr > 0, "no allowance for swap");
        require(allowedMgr >= swapMin, "allowance must bigger than min for swap");
        require(balanceOf(holder) >= allowedMgr, "not enough MGR to swap");

        uint256 heldMgr = swaps[holder].mgr;
        uint256 requiredRod = swaps[holder].rod;

        require(heldMgr == mgr, "invalid allowance and amount for swap");
        require(allowedMgr == heldMgr, "invalid allowance and amount for swap");
        require(requiredRod == rod, "incorrect rod value for mgr holder");

        return allowedMgr;
    }

    function swapByRODCA(
        address mgrHolder,
        uint256 mgr,
        address rodHolder,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) {
        uint256 allowedMgr = checkSwap(mgrHolder, mgr, rod);

        uint256 forOwner = allowedMgr * swapFee / 10000;
        uint256 forRodHolder = allowedMgr - forOwner;
        
        releaseHold(mgrHolder);

        if (forOwner > 0) {
            _transfer(mgrHolder, owner(), forOwner);
        }

        _transfer(mgrHolder, rodHolder, forRodHolder);

        emit Swapped(mgrHolder, mgr, rodHolder, rod);
    }

    function swap(
        address rodHolder,
        uint256 rod,
        uint256 mgr
    ) public nonReentrant {
        address mgrHolder = _msgSender();
        require(rodHolder != mgrHolder, "cannot swap myself");
        require(rodHolder != address(0), "invalid address to transfer");
        require(mgr >= swapMin, "mgr must bigger than minimum value for swap");
        require(balanceOf(mgrHolder) >= mgr, "not enough MGR to swap");
                
        (bool swapResult,) = address(RODCA).call(abi.encodeWithSignature("swapByMGRCA(address,uint256,address,uint256)", rodHolder, rod, mgrHolder, mgr));
        
        require(swapResult, "failed to call swapByMGRCA() in ROD contract");
        
        uint256 forOwner = mgr * swapFee / 10000;
        uint256 forRodHolder = mgr - forOwner;

        if (forOwner > 0) {
            transfer(owner(), forOwner);
        }

        transfer(rodHolder, forRodHolder);
        
        emit Swapped(mgrHolder, mgr, rodHolder, rod);
    }

}