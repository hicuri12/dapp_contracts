// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

contract MetagaugeNFT is ERC721Enumerable, Ownable, AccessControl, ReentrancyGuard {
    using Strings for uint256;

    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    uint256 private minPrice;
    uint256 private RODTransferFee;
    uint256 private BNBTransferFee;
    address private MGRCA;
    address private RODCA;

    mapping(uint256 => string) private _tokenURIs;

    mapping(string => AdminSale) private adminSales;
    mapping(uint256 => Sale) private sales;

    constructor() ERC721("Metagauge NFT", "MGN") {
        _transferOwnership(_msgSender());
        setAdminRole(_msgSender());
        minPrice = 10 ** 15;
        RODTransferFee = 500;
        BNBTransferFee = 250;
        adminSales["PB"] = AdminSale(false, new address[](0), new address[](0), 0, 0, 0);
        adminSales["OG"] = AdminSale(false, new address[](0), new address[](0), 0, 0, 0);
        adminSales["WL"] = AdminSale(false, new address[](0), new address[](0), 0, 0, 0);
        adminSales["AD"] = AdminSale(false, new address[](0), new address[](0), 0, 0, 0);
    }

    struct AdminSale {
        bool status;
        address[] members;
        address[] buyers;
        uint256 price;
        uint256 amountPerMember;
        uint256 totalAmount;
    }

    struct Sale {
        bool status;
        bool isAuction;
        address manager;
        string unit;
        uint256 price;
        uint256 minBidAmount;
        address topBidder;
        uint256 topBidAmount;
        
    }

    event SaleTransferred(string unit, uint256 price, address topBidder, uint256 topBidAmount);

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721Enumerable, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        _requireMinted(tokenId);

        string memory _tokenURI = _tokenURIs[tokenId];
        string memory base = _baseURI();

        if (bytes(base).length == 0) {
            return _tokenURI;
        }
        if (bytes(_tokenURI).length > 0) {
            return string(abi.encodePacked(base, _tokenURI));
        }

        return super.tokenURI(tokenId);
    }

    function _setTokenURI(uint256 tokenId, string memory _tokenURI) internal virtual {
        require(_exists(tokenId), "ERC721URIStorage: URI set of nonexistent token");
        _tokenURIs[tokenId] = _tokenURI;
    }

    function _burn(uint256 tokenId) internal virtual override {
        super._burn(tokenId);

        if (bytes(_tokenURIs[tokenId]).length != 0) {
            delete _tokenURIs[tokenId];
        }
    }

    receive() external payable {}
    fallback() external payable {}

    function setAdminRole(address to) public onlyOwner {
        _grantRole(ADMIN_ROLE, to);
    }

    function revokeAdminRole(address to) public onlyOwner {
        _revokeRole(ADMIN_ROLE, to);
    }

    function setCA(
        address _MGRCA,
        address _RODCA
    ) public onlyOwner {

        if (MGRCA != _MGRCA) {
            revokeAdminRole(MGRCA);
            setAdminRole(_MGRCA);

            MGRCA = _MGRCA;
        }

        if (RODCA != _RODCA) {
            revokeAdminRole(RODCA);
            setAdminRole(_RODCA);

            RODCA = _RODCA;
        }
    }

    function getCA() public view returns (address, address) {
        return (MGRCA, RODCA);
    }

    function setOptions(
        uint256 _minPrice,
        uint256 _RODTransferFee,
        uint256 _BNBTransferFee
    ) public onlyOwner {
        minPrice = _minPrice;
        RODTransferFee = _RODTransferFee;
        BNBTransferFee = _BNBTransferFee;
    }

    function getOptions() public view returns (uint256, uint256, uint256) {
        return (minPrice, RODTransferFee, BNBTransferFee);
    }

    function setAdminSale(
        string memory saleType,
        uint256 status,
        uint256 price,
        uint256 totalAmount
    ) public onlyOwner {
        require(status < 2, "invalid status value");

        if (keccak256(abi.encode(saleType)) != keccak256(abi.encode("AD"))) {
            require(price >= minPrice, "price must bigger than minimum price");
        }

        bool adminSaleStatus = false;

        if (status == 1) {
            adminSaleStatus = true;
        }

        adminSales[saleType].status = adminSaleStatus;
        adminSales[saleType].price = price;
        adminSales[saleType].totalAmount = totalAmount;
    }

    function setAdminSaleForMembers(
        string memory saleType,
        uint256 status,
        address[] memory members,
        uint256 price,
        uint256 amountPerMember,
        uint256 totalAmount
    ) public onlyOwner {
        require(status < 2, "invalid status value");

        if (keccak256(abi.encode(saleType)) != keccak256(abi.encode("AD"))) {
            require(price >= minPrice, "price must bigger than minimum price");
        }
        
        bool adminSaleStatus = false;

        if (status == 1) {
            adminSaleStatus = true;
        }
        
        adminSales[saleType].status = adminSaleStatus;
        adminSales[saleType].members = members;
        adminSales[saleType].price = price;
        adminSales[saleType].amountPerMember = amountPerMember;
        adminSales[saleType].totalAmount = totalAmount;
    }

    function initializeAdminSale(
        string memory saleType
    ) public onlyOwner {
        adminSales[saleType] = AdminSale(false, new address[](0), new address[](0), 0, 0, 0);
    }

    function getAdminSale(string memory key) public view returns (AdminSale memory) {
        return adminSales[key];
    }

    function _mint(
        address to,
        uint256 tokenId,
        string memory uri
    ) internal virtual {
        super._mint(to, tokenId);

        _setTokenURI(tokenId, uri);
    }

    function _transfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override {
        require(!sales[tokenId].status, "cannot transfer token during sell on market");

        super._transfer(from, to, tokenId);
    }
    
    function mintByAdmin(
        address to,
        uint256 tokenId,
        string memory uri
    ) public onlyRole(ADMIN_ROLE) {
        _mint(to, tokenId, uri);
    }

    function transferByAdmin(
        address holder,
        address to,
        uint256 tokenId
    ) public onlyRole(ADMIN_ROLE) {
        _transfer(holder, to, tokenId);
    }

    function mintWithRODByAdmin(
        address to,
        uint256 rod,
        uint256 tokenId,
        string memory uri
    ) public onlyRole(ADMIN_ROLE) {
        address sender = _msgSender();
        (bool success,) = address(RODCA).call(abi.encodeWithSignature("transferByAdmin(address,address,uint256)", to, sender, rod));
        require(success, "failed to call transferByAdmin() in ROD contract");

        mintByAdmin(to, tokenId, uri);
    }

    function transferWithRODByAdmin(
        uint256 tokenId,
        address holder,
        address buyer,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) {
        (bool success,) = address(RODCA).call(abi.encodeWithSignature("transferByAdmin(address,address,uint256)", buyer, holder, rod));
        require(success, "failed to call transferByAdmin() in ROD contract");
        
        transferByAdmin(holder, buyer, tokenId);
    }

    function airdropByAdmin(
        address to,
        uint256 tokenId,
        string memory uri
    ) public onlyRole(ADMIN_ROLE) {
        AdminSale memory adminSale = getAdminSale("AD");
        require(adminSale.status, "admin airdrop closed");
        require(adminSale.totalAmount > 0, "amount sold out");
        require(!_exists(tokenId), "token id already exists");
        
        _mint(to, tokenId, uri);

        adminSales["AD"].totalAmount = adminSale.totalAmount - 1;
    }

    function buyAdminSale(
        string memory saleType,
        uint256 tokenId,
        string memory uri
    ) public payable nonReentrant {
        AdminSale memory adminSale = getAdminSale(saleType);
        uint256 bnb = msg.value;
        address buyer = _msgSender();
        require(buyer.balance >= bnb, "not enough BNB");
        require(adminSale.status, "not on sale");
        require(bnb == adminSale.price, "incorrect BNB value for sale");
        require(adminSale.totalAmount > 0, "sold out");
        require(!_exists(tokenId), "token id already exists");
        require(adminSale.members.length == 0, "sale not for public");
        
        (bool sendToOwner,) = payable(owner()).call{value: bnb}("");
        require(sendToOwner, "failed to send BNB for owner");

        if (adminSale.amountPerMember > 0) {
            uint256 buyCount;
            for (uint256 n; n < adminSale.buyers.length; n ++) {
                if (adminSale.buyers[n] == buyer) {
                    buyCount ++;
                }
            }
            require(buyCount < adminSale.amountPerMember, "already buyed as limit");
        }

        _mint(buyer, tokenId, uri);

        adminSales[saleType].buyers.push(buyer);
        adminSales[saleType].totalAmount = adminSale.totalAmount - 1;
    }

    function buyAdminSaleByMember(
        string memory saleType,
        uint256 tokenId,
        string memory uri
    ) public payable nonReentrant {
        AdminSale memory adminSale = getAdminSale(saleType);
        uint256 bnb = msg.value;
        address buyer = _msgSender();
        require(buyer.balance >= bnb, "not enough BNB");
        require(adminSale.status, "not on sale");
        require(bnb == adminSale.price, "incorrect bnb value for sale");
        require(adminSale.totalAmount > 0, "sold out");
        require(!_exists(tokenId), "token id already exists");
        require(adminSale.members.length > 0, "cannot find member set");
        
        bool isMember = false;
        for (uint256 i; i < adminSale.members.length; i ++) {
            if (adminSale.members[i] == buyer) {
                isMember = true;
            }
        }
        require(isMember, "buyer is not member for this sale");

        if (adminSale.amountPerMember > 0) {
            uint256 buyCount;
            for (uint256 n; n < adminSale.buyers.length; n ++) {
                if (adminSale.buyers[n] == buyer) {
                    buyCount ++;
                }
            }
            require(buyCount < adminSale.amountPerMember, "already buyed as limit");
        }

        (bool sendToOwner,) = payable(owner()).call{value: bnb}("");
        require(sendToOwner, "failed to send BNB for owner");

        _mint(buyer, tokenId, uri);

        adminSales[saleType].buyers.push(buyer);
        adminSales[saleType].totalAmount = adminSale.totalAmount - 1;
    }

    function setSale(
        uint256 tokenId,
        bool status,
        bool isAuction,
        address manager,
        string memory unit,
        uint256 price,
        uint256 minBidAmount,
        address topBidder,
        uint256 topBidAmount
    ) internal {
        require(_exists(tokenId), "token not exists");
        
        sales[tokenId] = Sale(status, isAuction, manager, unit, price, minBidAmount, topBidder, topBidAmount);
    }

    function switchSaleStatus(
        uint256 tokenId
    ) public onlyRole(ADMIN_ROLE) {
        Sale memory sale = getSale(tokenId);
        require(bytes(sale.unit).length > 0, "token not on sale");
        
        if (sale.status) {
            sales[tokenId].status = false;
        }
        if (!sale.status) {
            sales[tokenId].status = true;
        }
    }

    function unregisterSaleByHolder(
        uint256 tokenId
    ) public {
        Sale memory sale = getSale(tokenId);
        address holder = _msgSender();
        require(ownerOf(tokenId) == holder, "only holder can unregister sale");
        require(sale.status, "token not on sale");
        require(sale.topBidAmount == 0, "cannot cancel after bid");
        
        setSale(tokenId, false, false, address(0), "", 0, 0, address(0), 0);
    }

    function getSale(
        uint256 tokenId
    ) public view returns (Sale memory) {
        return sales[tokenId];
    }

    function calculateSaleFee(
        uint256 tokenId
    ) public view returns (uint256, uint256) {
        Sale memory sale = getSale(tokenId);
        require(sale.status, "token not on sale");
        uint256 value;
        uint256 fee;
        uint256 result;

        if (sale.isAuction) {
            value = sale.topBidAmount;
        } else {
            value = sale.price;
        }

        if (keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("ROD"))) {
            fee = value * RODTransferFee / 10000;
            result = value - fee;
        }

        if (keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("BNB"))) {
            fee = value * BNBTransferFee / 10000;
            result = value - fee;
        }

        return (fee, result);
    }

    function registerOnMarketByAdmin(
        address seller,
        uint256 tokenId,
        uint256 isAuction,
        address manager,
        string memory unit,
        uint256 price,
        uint256 minBidAmount
    ) public onlyRole(ADMIN_ROLE) {
        require(ownerOf(tokenId) == seller, "seller is not owner of token");
        require(!sales[tokenId].status, "already on sale");
        require(isAuction < 2, "invalid auction value");
        bool auction = false;

        if (isAuction == 1) {
            require(hasRole(ADMIN_ROLE, manager), "invalid manager address");
            require(minBidAmount >= minPrice, "bid amount must bigger than minimum price");

            auction = true;
        }

        if (isAuction == 0) {
            require(price >= minPrice, "price must bigger than minimum price");
        }

        setSale(tokenId, true, auction, manager, unit, price, minBidAmount, address(0), 0);
    }

    function bidOnMarket(
        uint256 tokenId
    ) public payable nonReentrant {
        Sale memory sale = getSale(tokenId);
        address bidder = _msgSender();
        address seller = ownerOf(tokenId);
        uint bnb = msg.value;
        require(bidder != seller, "already holding token");
        require(bidder.balance >= bnb, "not enough BNB to bid");
        require(sale.status, "token not on sale");
        require(sale.isAuction, "type of sale is not auction");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("BNB")), "sale does not for BNB");
        require(bnb >= sale.minBidAmount, "BNB must bigger than minimum amount for bidding");
        require(bnb > sale.topBidAmount, "BNB must bigger than current top bid amount");

        if (sale.price > 0) {
            require(bnb <= sale.price, "cannot bid BNB bigger than price");
        }
        
        sales[tokenId].topBidder = bidder;
        sales[tokenId].topBidAmount = bnb;
   
        uint256 forManager = bnb - sale.topBidAmount;
        
        (bool sendToManager,) = payable(sale.manager).call{value: forManager}("");
        require(sendToManager, "failed to send BNB for owner");

        if (sale.topBidAmount > 0) {
            (bool sendToCurrentTopBidder,) = payable(sale.topBidder).call{value: sale.topBidAmount}("");
            require(sendToCurrentTopBidder, "failed to send BNB for current top bidder");
        }
    }

    function bidOnMarketByRODCA(
        uint256 tokenId,
        address bidder,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) returns (address, uint256, address) {
        Sale memory sale = getSale(tokenId);
        address seller = ownerOf(tokenId);
        require(bidder != seller, "already holding token");
        require(sale.status, "token not on sale");
        require(sale.isAuction, "type of sale is not auction");
        require(hasRole(ADMIN_ROLE, sale.manager), "invalid manager address");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("ROD")), "sale does not for ROD");
        require(rod >= sale.minBidAmount, "ROD must bigger than minimum amount for bidding");
        require(rod > sale.topBidAmount, "ROD must bigger than current top bid amount");

        if (sale.price > 0) {
            require(rod <= sale.price, "cannot bid ROD bigger than price");
        }

        sales[tokenId].topBidder = bidder;
        sales[tokenId].topBidAmount = rod;

        return (sale.topBidder, sale.topBidAmount, sale.manager);
    }
    
    function finishBidByAdmin(
        uint256 tokenId
    ) public payable onlyRole(ADMIN_ROLE) {
        Sale memory sale = getSale(tokenId);
        (, uint256 forSeller) = calculateSaleFee(tokenId);
        require(_msgSender() == sale.manager, "only manager can finish auction");
        require(msg.value == forSeller, "invalid BNB value for seller");
        require(sale.status, "token not on sale");
        require(sale.isAuction, "type of sale is not auction");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("BNB")), "sale does not for BNB");
        require(sale.topBidder !=  address(0), "no bid for sale");
        
        setSale(tokenId, false, false, address(0), "", 0, 0, address(0), 0);

        if (sale.topBidAmount > 0) {
            address seller = ownerOf(tokenId);
            _transfer(seller, sale.topBidder, tokenId);

            (bool sendToSeller,) = payable(seller).call{value: forSeller}("");
            require(sendToSeller, "failed to send BNB for seller");

            emit SaleTransferred(sale.unit, sale.price, sale.topBidder, sale.topBidAmount);
        }   
    }

    function finishRODBidByAdmin(
        uint256 tokenId
    ) public onlyRole(ADMIN_ROLE) {
        Sale memory sale = getSale(tokenId);
        (, uint256 forSeller) = calculateSaleFee(tokenId);
        require(_msgSender() == sale.manager, "only manager can finish auction");
        require(sale.status, "token not on sale");
        require(sale.isAuction, "type of sale is not auction");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("ROD")), "sale does not for ROD");
        require(sale.topBidder !=  address(0), "no bid for sale");

        setSale(tokenId, false, false, address(0), "", 0, 0, address(0), 0);

        if (sale.topBidAmount > 0) {
            address seller = ownerOf(tokenId);
            _transfer(ownerOf(tokenId), sale.topBidder, tokenId);

            (bool sendToSeller,) = address(RODCA).call(abi.encodeWithSignature("transferByAdmin(address,address,uint256)", sale.manager, seller, forSeller));
            require(sendToSeller, "failed to send BNB for seller");

            emit SaleTransferred(sale.unit, sale.price, sale.topBidder, sale.topBidAmount);
        }
    }

    function transferOnMarket(
        uint256 tokenId
    ) public payable nonReentrant {
        Sale memory sale = getSale(tokenId);
        uint256 bnb = msg.value;
        address buyer = _msgSender();
        address seller = ownerOf(tokenId);
        require(buyer != seller, "already holding token");
        require(sale.status, "token not on sale");
        require(!sale.isAuction, "type of sale is auction");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("BNB")), "sale does not for BNB");
        require(sale.price == bnb, "incorrect amount of BNB");
        require(buyer.balance >= sale.price, "not enough BNB to transfer");

        (uint256 forOwner, uint256 forSeller) = calculateSaleFee(tokenId);

        if (forOwner > 0) {
            (bool sendToOwner,) = payable(owner()).call{value: forOwner}("");
            require(sendToOwner, "failed to send BNB for owner");
        }
        
        (bool sendToSeller,) = payable(seller).call{value: forSeller}("");
        require(sendToSeller, "failed to send BNB for seller");
        
        setSale(tokenId, false, false, address(0), "", 0, 0, address(0), 0);
        _transfer(seller, buyer, tokenId);

        emit SaleTransferred(sale.unit, sale.price, buyer, sale.price);
    }

    function transferOnMarketByRODCA(
        uint256 tokenId,
        address buyer,
        uint256 rod
    ) public onlyRole(ADMIN_ROLE) returns (uint256, uint256, address) {
        Sale memory sale = getSale(tokenId);
        address seller = ownerOf(tokenId);
        require(buyer != seller, "already holding token");
        require(sale.status, "token not on sale");
        require(!sale.isAuction, "type of sale is auction");
        require(keccak256(abi.encode(sale.unit)) == keccak256(abi.encode("ROD")), "sale does not for ROD");
        require(sale.price == rod, "incorrect amount of ROD");
        
        (uint256 forOwner, uint256 forSeller) = calculateSaleFee(tokenId);

        setSale(tokenId, false, false, address(0), "", 0, 0, address(0), 0);
        _transfer(seller, buyer, tokenId);

        emit SaleTransferred(sale.unit, sale.price, buyer, sale.price);

        return (forOwner, forSeller, seller);
    }
}